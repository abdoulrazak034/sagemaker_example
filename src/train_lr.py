# train.py

import os
import argparse
import pandas as pd
import joblib
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report

def load_data(data_dir):
    train_data = pd.read_csv(os.path.join(data_dir, 'train-V-1.csv'))
    test_data = pd.read_csv(os.path.join(data_dir, 'test-V-1.csv'))
    return train_data, test_data

def preprocess_data(train_data, test_data):
    X_train = train_data.drop('price_range', axis=1)
    Y_train = train_data['price_range']
    X_test = test_data.drop('price_range', axis=1)
    Y_test = test_data['price_range']
    
    # Standardize the features
    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)
    
    return X_train, Y_train, X_test, Y_test

def train_model(X_train, Y_train):
    model = LogisticRegression()
    model.fit(X_train, Y_train)
    return model

def evaluate_model(model, X_test, Y_test):
    predictions = model.predict(X_test)
    accuracy = accuracy_score(Y_test, predictions)
    conf_matrix = confusion_matrix(Y_test, predictions)
    class_report = classification_report(Y_test, predictions)
    
    print(f"Accuracy: {accuracy}")
    print(f"Confusion Matrix:\n{conf_matrix}")
    print(f"Classification Report:\n{class_report}")

def save_model(model, model_dir):
    joblib.dump(model, os.path.join(model_dir, 'model.joblib'))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    
    # Sagemaker specific arguments
    parser.add_argument('--data-dir', type=str, default='/opt/ml/input/data')
    parser.add_argument('--model-dir', type=str, default='/opt/ml/model')
    parser.add_argument('--output-dir', type=str, default='/opt/ml/output')
    
    args = parser.parse_args()
    
    train_data, test_data = load_data(args.data_dir)
    X_train, Y_train, X_test, Y_test = preprocess_data(train_data, test_data)
    
    model = train_model(X_train, Y_train)
    
    evaluate_model(model, X_test, Y_test)
    
    save_model(model, args.model_dir)
