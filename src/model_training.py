# Importing Libraries
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report
from .data_preprocessing import preprocess_data
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import joblib

def train_and_evaluate_model(model, X_train, Y_train, X_test, Y_test):
    model.fit(X_train, Y_train)
    predictions = model.predict(X_test)
    accuracy = accuracy_score(Y_test, predictions)
    conf_matrix = confusion_matrix(Y_test, predictions)
    class_report = classification_report(Y_test, predictions)
    
    return accuracy, conf_matrix, class_report, predictions

def main():
    # Load preprocessed data
    X_train, X_test, Y_train, Y_test = preprocess_data("C:/Users/abdoul.mounkaila/Downloads/sagemaker_example/data/mobile_price_range_data.csv")
    # Dictionary to store models and their results
    models = {
        'Logistic Regression': LogisticRegression(),
        'K-Nearest Neighbors': KNeighborsClassifier(n_neighbors=15),
        'SVM Linear': SVC(kernel='linear', C=0.01),
        'SVM RBF': SVC(kernel='rbf', C=1),
        'Decision Tree': DecisionTreeClassifier(criterion='entropy'),
        'Random Forest': RandomForestClassifier(n_estimators=70, criterion='entropy', max_depth=6)
    }
    
    results = {}

    # Train and evaluate each model
    for model_name, model in models.items():
        accuracy, conf_matrix, class_report, predictions = train_and_evaluate_model(model, X_train, Y_train, X_test, Y_test)
        results[model_name] = {
            'accuracy': accuracy,
            'conf_matrix': conf_matrix,
            'class_report': class_report,
            'predictions': predictions
        }
        print(f"{model_name} - Accuracy: {accuracy}")
        print(f"Confusion Matrix:/n{conf_matrix}")
        print(f"Classification Report:/n{class_report}")

    # Plotting the results
    plt.figure(figsize=(10, 5))
    model_names = list(results.keys())
    accuracies = [results[model]['accuracy'] for model in model_names]
    plt.bar(model_names, accuracies, color='skyblue')
    plt.xlabel("Algorithm")
    plt.ylabel("Accuracy Score")
    plt.title("Model Comparison")
    plt.xticks(rotation=45)
    plt.show()

    # Save the model with the best accuracy
    best_model_name = max(results, key=lambda k: results[k]['accuracy'])
    best_model = models[best_model_name]
    joblib.dump(best_model, f'models/best_model_{best_model_name}.joblib')
    print(f"Best model saved: {best_model_name}")

