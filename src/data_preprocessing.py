# Importing Libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

def preprocess_data(file_path):
    # Load the dataset
    data = pd.read_csv(file_path)
    print("First 5 rows of the dataset:")
    print(data.head())

    # Removing null values
    if data.isnull().sum().sum() > 0:
        data = data.dropna()
        print("Null values found and removed")
    else:
        print("No null values found")

    # Dataset info
    print("Dataset Information:")
    print(data.info())

    # Defining X and Y
    X = data.drop('price_range', axis=1)
    Y = data['price_range']

    # Splitting data into training and test data
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.30, random_state=42)
    print(f"Training data shape: {X_train.shape}")
    print(f"Testing data shape: {X_test.shape}")

    # Standardizing the features
    sc = StandardScaler()
    X_train = sc.fit_transform(X_train)
    X_test = sc.transform(X_test)

    return X_train, X_test, Y_train, Y_test

if __name__ == "__main__":
    # Example usage
    X_train, X_test, Y_train, Y_test = preprocess_data('../data/raw/mobile_price_range_data.csv')
