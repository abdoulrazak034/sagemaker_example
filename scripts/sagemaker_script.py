# model_training.py

# Importing Libraries
import boto3
import sagemaker
from sagemaker.estimator import Estimator
from src.data_preprocessing import preprocess_data
import os

def main():
    # Define the S3 key prefix and region
    sk_prefix = "sagemaker/mobile_price_range_data"
    region_name = "eu-north-1"
    bucket = 'abdoulbucketsagemaker'
    
    # Initialize boto3 and sagemaker sessions
    sm_boto3 = boto3.client("sagemaker", region_name=region_name)
    sess = sagemaker.Session(boto_session=boto3.Session(region_name=region_name))
    region = sess.boto_session.region_name
    
    print(f'Using bucket {bucket}')
    
    # Upload the files to S3 bucket using SageMaker's upload_data method
    train_path = sess.upload_data(path="../data/train-V-1.csv", bucket=bucket, key_prefix=sk_prefix)
    test_path = sess.upload_data(path="../data/test-V-1.csv", bucket=bucket, key_prefix=sk_prefix)
    print(f'Training data path: {train_path}')
    print(f'Testing data path: {test_path}')
    
    # Define the Estimator
    estimator = Estimator(
        entry_point='train.py',
        source_dir='src',  # Directory where train.py is located
        image_uri='your-docker-image-uri',  # Use an appropriate Docker image URI for your environment
        role='your-sagemaker-role',  # Replace with your SageMaker execution role ARN
        instance_count=1,
        instance_type='ml.m5.large',
        output_path=f's3://{bucket}/output',
        sagemaker_session=sess
    )

    # Set hyperparameters (if any)
    hyperparameters = {
        's3-bucket': bucket,
        's3-key': 'data/your_data.csv'
    }

    # Launch the training job
    estimator.fit(inputs=f's3://{bucket}/data', hyperparameters=hyperparameters)
    print("SageMaker training job launched successfully.")

if __name__ == "__main__":
    main()
